# Algoritmusok

* Számítási lépések jól definiált sorozata illetve egymásba ágyazása a strukturált programozás tanult eszközeivel - azaz ebben a félévben ezen alakzatok használatával:
	* Üres utasítás (SKIP)
	* Értékadás (változó = típushelyes kifejezés)
	* Alprogram-hívás (egy már definiált alprogram neve, majd aktuális paraméterlistája - függvény vagy eljárás; ha önmaga más paraméterekkel: rekurzió)
	* Szekvencia (két elemibb program egymás után végrehajtva)
	* Ciklus (elöltesztelős: while; számlálós: for)
	* Elágazás (if; multi-if)
	* return utasítás
* C++-ban kb. ami függvény. Operátorok is ide tartoznak, de pl. a getter-setter metódusok nem (azok technikai, a paradigmából adódó elemek)
* Minden művelet (azaz metódus) függvény is, de vannak típuson kívüli, globális függvények is
* Bemenete és kimenete van
    * Ha metódus, a bemenet impliciten tartalmaz egy példányt az adott típuson
	* A bemenetre előfeltételt adunk meg (egy logikai függvény, állítás), azaz csak azokra az inputokra kell helyesen működnie, amik az EF-t teljesítik
	* Az algoritmus "helyessége" pedig azt jelenti, ha egy adott megfelelő bemenetre a futása után a kimeneten előálló értékekre igaz az utófeltétel. Azaz az utófeltétel az előfeltételnek megfelelő bemenet és az abból előálló kimenet között fogalmaz meg kapcsolatot, elvárást
    * Az utófeltételt nem fogjuk formálisan leírni, mert ebben a tárgyban nem formális eszközökkel akarunk helyességet bizonyítani, programot szintetizálni
    * Azon logikai állítást, amit az algoritmusnak meg kell tartania, azaz az EF és az UF részei is, invariánsnak nevezzük. A típusinvariáns pedig egy olyan állítás, ami egy típus összes művelete által helyben hagyandó
    * Egy paraméter lehet akár egyszerre bemenő ÉS kimenő típusú is - elvi szinten ez a mellékhatásos függvény, vagy eljárás; gyakorlati szinten ehhez szoktuk használni a referenciákat
* Ezen az órán struktogrammal adjuk meg, de megadható lenne pszeudokóddal, folyamatábrával, konkrét nyelven, bináris alakban is. Maga az algoritmus lényege független a leírásának módjától
* Fogalmak:
    * Algoritmus (algorithm): minden, ami számítási folyamat
    * Művelet/metódus (operation/method): olyan algoritmus, amit egy adott adatszerkezet osztályában kódolnál le
    * Eljárás (procedure): aminek a visszatérési típusa void (azaz nem tér vissza semmivel), ellenben valamit manipulál az állapottéren, azaz mellékhatása van
    * Tiszta függvény (pure function): mellékhatás nélküli függvény, ami az inputból outputot csinál és más semmit
    * Mellékhatásos függvény (function with side effects): az előző kettő keveréke, csinál is valamit (kiír valamit a konzolra például) és ki is számol valamit, amivel visszatér
    * Struktogram: egy algoritmus (legyen az akár művelet, függvény, eljárás, bármi) egyik megadási módja
* Fontos faktor a helyesség mellett a hatékonyság: időigény, tárigény
	* Abszolút értelemben egyiket se tudjuk megadni, hiszen az algoritmusokat általánosan fogalmazzuk meg (nem ismerjük se a gépet, amin fut, se a körülményeket, a bemenetet, se semmit)
	* Tárigényt meg tudjuk adni a bemenet arányában ("n méretű tömb rendezéséhez kell-e vajon egy másik n méretű segédtömb?"). Ezek az input, output, segédváltozók
	* Időigényt (lásd még: bonyolultság, komplexitás) pedig a lépésszámmal tudjuk közelíteni
		* Megállapítjuk, mi a domináns művelet (az az, ami sokszor és drágán fut le) és megnézzük n függvényében (ez a bemenet mérete) ez a művelet hányszor fut le
		* Ez egy konkrét függvény lesz, amiket majd nagyságrendjük alapján függvényosztályokba soroljuk
* Formális paraméterlistának hívjuk a struktogram fejében megadott paraméterlistát. Ott bevezetünk változókat, amelyikekkel majd a paraméterként megadott értékeket vagy referenciákat fogjuk hivatkozni az algoritmus törzsében
    * Alakja: zárójelben "változónév : típus"-párok, vesszővel elválasztva - lehet üres is
* Aktuális paraméterlista az adott hívásnál megadott paraméterek összessége. Itt megadhatunk literálokat (konkrét értékek), változókat, de akár összetett kifejezéseket is, amik be fognak helyettesítődni értékükkel vagy referenciájukkal a hívás helyére
    * A formális és az aktuális paraméterlista i. elemének azonos típusúnak kell lennie (ha az aktuális paraméter egy változó, a névnek természetesen nem kell megegyeznie)
* Ha referenciát várunk, akkor azt a formális paraméterlistában jelöljük egy & jellel
	* Az elemi (primitív) típusoknál van csak ez a megkülönböztetés, az összetett adatszerkezetek változóit mindig referencia szerint adjuk át (nyilván így cselekszünk, hiszen ezek potenciálisan nagy méretű változók, felesleges őket lemásolni)
	* A referencia szerinti paraméterátadásnál nem másoljuk le az átadott adatot a hívás helyén, hanem ténylegesen ugyanazt a példányt használjuk: minden rajta elvégzett módosítás kifelé is hat (mellékhatásos függvény)
	* Az érték szerinti paraméterátadás esetén minden az alprogramban eszközölt módosítás hatástalan az eredeti változóra, ami az aktuális paraméterlistában szerepel, a függvény hívása után ezek a változások elvesznek (kivéve ha nem térünk vissza vele, stb.), úgy vesszük ugyanis, hogy ha pl. egy változót adtunk meg paraméternek, előbb annak az értékét lemásoljuk és magával az értékkel dolgozunk tovább, ami persze független az őt eredetileg tartalmazó változótól
	* Értelemszerűen skalár értékű kifejezés ill. literál nem kerülhet egy aktuális paraméterlistába referencia szerint átadandó paraméter helyébe, ugyanis az ilyen kifejezések mögött nincs változó, amire referenciát lehetne állítani
	* A referenciajelet behelyettesítéskor (aktuális paraméter) nem írjuk ki
	    * Ha referenciát akarnánk átadni, azt a * jellel oldhatjuk meg, de nem fogunk ilyesmit használni (emellett a * jellel fogjuk a pointereket is megadni, ez már egy gyakrabban előforduló eset lesz)
	* Ha egy primitív típusú változót nem akarunk egy algoritmusban módosítani, akkor kifejezetten hibának számít referencia szerint átadni, mivel a programozási nyelvekben ez a paraméterátadási mód általában drágább, mint az érték szerinti
    * A tömb is adatszerkezetnek számít, de paraméterátadásnál úgy vesszük, hogy érték szerint adjuk át az első elem referenciáját
        * Azaz, ha t egy paraméterként átadott tömb, t2 egy másik tömb, akkor a t[0] = 1 hatni fog kifelé, a t = t2 nem, hacsak nem raktuk ki a referenciajelet a t elé