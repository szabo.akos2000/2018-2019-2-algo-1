# Összehasonlító rendezések - Maximumkiválasztó rendezés

## Tömbös változat

* Kis kitérő, hogy jobban megértsük a láncolt megvalósítást:

```
maximumSearchSort(a : T[n])
	i = 1 to n-1
		max = a[1]
		ind = 1
		j = 2 to n-i+1
			HA a[j] > max
				max = a[j]
				ind = j
		a[ind] = a[n-i+1]
		a[n-i+1] = max
```

* Az algoritmus alapja, hogy összesen n-1 alkalommal lefuttassuk a maximumkiválasztást a tömb elejére, majd a maximum elemet a végére pakoljuk
	* Első alkalommal futtassuk tehát le az egész tömbre, majd cseréljük ki az utolsó elemmel a kapott maximumot
	* Második körben csak [1..n-1]-ig menjünk, majd az n-1. elemmel cseréljük ki, hiszen ez már csak az egész, eredeti tömbben globálisan a második legnagyobb elem lehet
	* Így haladhatunk, végül marad egy elemű tömbprefixünk, ami gyakorlatilag annyit jelent, hogy az 1. és 2. elemeket megcseréljük, ha szükséges
	* Ezekről tudjuk, hogy ők a globális legkisebb két elem, mert ha nem így lett volna, már korábban kiválasztódtak volna. Ezért tehát ez az eljárás helyes
* Az algoritmus ciklusmagjának elején kijelöljük az első elemet maxnak, majd a többi elemmel hasonlítjuk ezt össze (azért megyünk 2-től a ciklusban)
	* Az egyből szemet szúrhat, hogy csak úgy meg mertük címezni a[1]-et. És mi van, ha a tömb üres? Maximumkiválasztás programozási tételnél ez mindig neuralgikus pont. Mivel 1..n-1-ig megy a ciklus, amibe márpedig beléptünk, n=0, de még csak n=1 esetén se tehettük volna ezt meg (és nagyon helyes is így, egy nulla vagy egy elemű tömb gyárilag rendezettnek tekinthető)
* A ciklus felső határa úgy jött ki, hogy tudjuk, minden körben egyre nőnie kell a rendezett suffixnek, de ez kezdetben még üres
	* Természetesen ahogy növeljük i-t, így fog nőni ez a rendezett rész, ezért van, hogy pont a belső ciklus felső határával cseréljük ki a maximális elemet
* A műveletigényről: a maximumkiválasztásról tudjuk, hogy Θ(n)-es, mivel az egy programozási tétel. Ezt hajtjuk végre n-1-szer, ezért ez Θ(n<sup>2</sup>)
