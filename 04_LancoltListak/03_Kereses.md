# Keresés egyszerű listában

* Egy alapfeladat következik
	* Adott egy k kulcs, állapítsuk meg, hogy az l-lel jelölt listában (l lesz a lista első elemére mutató pointer) szerepel-e olyan E1, aminek a kulcsa épp k
* Órán az alábbi verziót vettük:

	```
	keres(l : E1*, k : T) : L
	    p = l
	    AMÍG p != NULL
	        HA p->key == k
	            return true
	        KÜLÖNBEN
	            p = p->next
        return false
	```
* Az algoritmus kb. a lineáris keresésnek felel meg (bár annak kissé egyszerűbb implementációja)
* Az első sorban azért vezetünk be egy új pointert l értékére, mert valamivel muszáj bejárni a listát, de nem használhatjuk erre konkrétan l-t, mert azzal módosítanánk a lista értékét, na meg elszivárogna a memória
* p pointer kb. úgy viselkedik, mint egy hagyományos for ciklus i ciklusváltozója. Előbb inicializáljuk az kezdőértékre (ami l), ez olyan, mint az i=1 értékadás; majd addig megyünk, amíg még nem léptünk túl az utolsó elemen: amíg nem NULL. i esetében ez az lenne, hogy nem nagyobb, mint a tömb mérete. A ciklusmagban persze nem valaminek az i-edik elemét vizsgáljuk, hanem a p által mutatott elemet
* Az OOP óráról ismert felsoroló egy konkrét eseteként is felfogható ez: a láncolt lista felsorolójánál p=l a t.first(); p==NULL a t.end(); maga p a current() és p = p->next a továbblépés

## Órán nem szereplő plusz anyagok

### További variációk

* Ilyen kérdéseink lehetnek:
	* "Csak" annyi, hogy szerepel-e k l-ben
	* Ha szerepel, adjuk vissza az első ilyet, vagy NULL-t
	* Az utolsó ilyet
	* Az összes ilyet
	* Csak valamilyen adattagját
	* stb.

* Felírjuk a boolos és az első elemes változatokat:

	```
	keres(l : E1*, k : T) : L
	    p = l
	    AMÍG p != NULL es p->key != k
	        p = p->next
        return p != NULL
	```

* Valamint
	
	```
	keres(l : E1*, k : T) : E1*
	    p = l
	    AMÍG p != NULL es p->key != k
	        p = p->next
        return p
	```

* A while ciklus fejében kifejezetten nem mindegy a két konjunkcióban álló feltétel sorrendje. Használhatjuk és használjuk is az ún. lusta kiértékelést vagy rövidzárt, aminek az a lényege, ha egy éselés első tagja hamis, akkor tudván hogy az egész állítás hamis lesz, a másodikat már ki se számolja. Ezért lehet az, hogy le merhetünk írni olyat, hogy p->key, mert tudjuk, hogy nem lehet NullPointerException, mert akkor már megállt volna az első tagnál a kiértékelés. A ciklusfeltétel a fordított sorrendben megadva hibás lenne

### Keresés rendezett egyszerű listában

* Keressünk növekvően rendezett listában egy k kulcsú elemet
* Boollal vagy magával az elemmel térve vissza

	```
	keres(l : E1*, k : T) : L
	    p = l
	    AMÍG p != NULL es p->key < k
	        p = p->next
        return p!=NULL es p->key == k
	```

	* Azaz, addig megyünk, amíg még kisebb az aktuális listaelem a keresettnél
	* Amikor megállunk, az vagy azért történt, mert NULL lett (véget ért a lista), vagy mert nagyobb lett, mint k (ekkor a rendezettség miatt nem kell megnéznünk a maradékot), vagy mert épp megtaláltuk k-t. Ezt ellenőrizzük a végén, persze nullcheckkel
		
	```
	keres(l : E1*, k : T) : E1*
	    p = l
	    AMÍG p != NULL es p->key < k
	        p = p->next
        HA p != NULL es p->key = k
            return p
        KÜLÖNBEN
            return NULL
	```

	* Itt kicsit nehézkesebb a return, mert akkor is NULL-lal kell visszatérni, ha van ugyan értelmes p, de az nem a k kulcsú
