# Láncolt listák aritmetikai (tömbös) ábrázolása (reprezentációja)

* Maga a láncolt lista amellett, hogy egy interfész, egyben egy implementációt is jelent (láncolt megvalósítás)
* De a lista interfészt (azazhogy egy iterált és lineáris adatszerkezet, melynek van első eleme és tudunk rajta lépegetni, beszúrni egy elem mögé, kiláncolni egy elemet ideális műveletigényekkel) persze tömbökkel is szimulálhatjuk, így:
	* Legyen egy tömbünk, amiben párok vannak. A pár első eleme maga a listaelem, ha úgy tetszik a kulcs. A másik elem, pedig a "next", bár itt ez nem egy pointer, hanem egy tömbindex: ennyiedik helyen lesz a következő elem
	* Az utolsó elem index komponense a 0 (vagy ha 0-tól indexelnénk, lehet -1)
	* Tartozik még egy adattag a típushoz, az első elem tömbbeli indexe
* Azért jó ez a fajta megvalósítás az ArrayListhez/vectorhoz képest, mert ha a lista elejéről törlünk, nem kell balra shiftelni minden elemet, csak átállítjuk az első elem indexét
* Persze a lista bejárása így is viszonylag drága, mert mindig ki kell olvasni a következő indexeket
* Számon tarthatunk esetleg még egy indexváltozót, a szabad elemek listájának első elemét, ami egyfajta lista a listában, szintén 0-val terminálva
	* Ha új elemet kell beszúrni, erre az indexre rakjuk be, majd az utolsó listaelem "next"-jét átállítjuk erre, illetve frissítjük ennek az elemnek a nextjét és az üres elemek kezdőindexét is
	* Ehhez - ha konstans időt szeretnénk lista végére beszúráshoz - még az utolsó listaelem indexét is tárolni kell
	* Tehát: kell a tömb, az üres tömb (ami lehet fizikailag egy tömbben is!), kell a két tömb eleje és a kitöltött elemek tömbjének vége, így a megfelelő műveletek konstans műveletigénnyel működhetnek
	* Ez amúgy így konkrétan a "végelemes lista" implementációja
* Erre nincs egyféle "kanonizált" megoldás, hanem használatieset-függő a megvalósítás mikéntje
