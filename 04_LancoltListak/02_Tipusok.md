# Listatípusok

* Három, egymástól független faktorról beszélhetünk, amik alapján a világ listáit összesen 2<sup>3</sup>, azaz 8 típusba sorolhatjuk:
	* Fejelemes, vagy sem
	* Kétirányú, vagy sem
	* Ciklikus, vagy sem
* Ezeket a tulajdonságokat általában egy-egy betűvel vagy annak hiányával jelöljük
	* A fejelemes listák jele a H (header)
	* A kétirányúaké a 2-es szám (ha ki akarjuk emelni az egyirányúságot, írhatunk 1-est, de ha nem írunk semmit, akkor is egyirányú)
	* A ciklikusaké a C (cyclic)
* Maguk a listatípusok tehát így állnak elő, pl.:
	* "Fejelemes lista": HL (esetleg H1L)
	* "Fejelemes, kétirányú lista": H2L
	* "Kétirányú, ciklikus lista": 2CL
	* Valamint, a non plus ultra, a "fejelemes, kétirányú, ciklikus lista": H2CL
	* Ha egy listára egyik speciális tulajdonság se igaz, azt "egyszerű lista"-nak mondjuk, és SL-lel rövidítjük
* Lehet még egy lista rendezett vagy rendezetlen, de ez nem külön listatípus, hiszen ez szemantikai kategória
* Beszélhetünk még egyszeresen vagy többszörösen láncolt listáról, de ebben a félévben ezek nem kerülnek szóba
	* Egy példát azért mondok: legyen egy lista emberekkel, ami egyszerre rendezett névsor és magasság szerint is. Ezt úgy tudjuk megoldani, ha két "next" pointert is definiálunk, az egyik az egyik lista, a másik a másik lista szerint adja meg a rákövetkező elemet
* Ezen kívül lehet még egy lista a fejelemesség analógiájára "végelemes". Ezt se tekintjük külön listatípusnak, de később látunk majd rá példát