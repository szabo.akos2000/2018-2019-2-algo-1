# Kétirányú listák (2L - azaz S2L, H2L, stb.)

* A kétirányú lista elemeinek típusa az E2, ami egy háromkomponensű rekord
	* key : T, ezt ismerjük
	* next : E2*, ezt is
	* prev : E2*, egy mutató az előző elemre
* Egy sima 2L-t ugyanúgy egy l nevű pointerrel adhatunk meg, ami üres lista esetében NULL, különben egy E2-re mutat, aminek a prevje NULL
* A fejelemes kétirányú lista (H2L) esetében pedig az üres lista megfelelője egy E2 típusú definiálatlan kulccsal, NULL prevvel és NULL nexttel rendelkező elemre mutató pointer (fejelem), a nemüres fejelemes lista első értelmes eleme pedig természetesen visszamutat a fejelemre (aminek viszont már NULL a visszapointere)
* Az E2 konstruktora keyt nemdefiniáltnak hagyja, next és prev értéke viszont NULL lesz létrehozáskor
