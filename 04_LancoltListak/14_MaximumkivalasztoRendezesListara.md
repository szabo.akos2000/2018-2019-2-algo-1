# Összehasonlító rendezések - Maximumkiválasztó rendezés

## Láncolt változat - Fejelemes listára

* Annak ellenére, hogy fejelemes a lista, ezúttal két pointert fogunk használni, mert itt kell egy extra előre nézés

```
maximumSelectionSort(f : E1*)
	rl = NULL
	AMÍG f->next != NULL
		maxPrev = f
		max = f->next
		prev = f->next
		p = prev->next
		AMÍG p != NULL
			HA max->key < p->key
				maxPrev = prev
				max = p
			prev = p
			p = p->next
		maxPrev->next = max->next
		max->next = rl
		rl = max
	f->next = rl
```

* Az rl lesz egy rendezett (amúgy nem fejelemes) lista, amibe egyre csak gyűjteni fogjuk az elemeket, hasonlóan, mint a lista megfordítása és a beszúró rendezés feladatoknál
* A végén f mögé beállítjuk ezt a listát
* Itt tehát nem egy tömb hátsó indexeire, hanem egy új listába gyűjtjük a már jó helyen levő elemeket
	* Természetesen az újakat a lista elejére rakjuk, mivel így hatékonyabb és mivel tudjuk, hogy a korábban kiválasztottak nagyobbak voltak, azaz eléjük szúrhatunk biztonsággal
	* Ha nem így tennék, az kb. a beszúró rendezés lenne, kicsit rosszabb műveletigénnyel
* A külső ciklusban ahogy ezt a programozási tételből megismerhettük, elmentjük a maximumelemet (ami a lista első érdemi eleme), majd az ő rákövetkezőjét (ami már lehet null!) kinevezzük első vizsgálandó elemnek
* Végigmegyünk a (prev,p) kettőssel az elemeken, és ha a maxnál nagyobbat találok, frissítjük azt
* A végén a maxot kiláncolom (ezért kellett az előző), majd beláncolom a készülő listába
* A műveletigény természetesen a dupla ciklus miatt négyzetes

## Láncolt változat - H2CL-ra

* Itt újabb tabukat döntögetünk, mivel a fejelemesség ellenére referencia szerint adom át a fejelemet. Ez az algoritmus most úgy fog működni, hogy fejelemestül készíti el a rendezett, új listát, és a végén a fejelemet konkrétan átmutattaja erre

```
maximumSelectionSort(&f : E2C*)
	rf = new E2C
	AMÍG f->next != f
		max = f->next
		p = max->next
		AMÍG p != f
			HA p->key > max->key
				max = p
			p = p->next
		max->next->prev = max->prev
		max->prev->next = max->next
		HA rf->next == rf
			max->next = rf
			rf->prev = max
		KÜLÖNBEN
			max->next = rf->next
			rf->next->prev = max
		rf->next = max
		max->prev = rf
	q = f
	f = rf
	delete q
```

* Az elején tehát létrehozunk egy új node-ot, amit fejelemnek szánunk, ezért a kulcsa definiálatlan, de a két pointere saját magára kell mutasson
	* Az E2C default konstruktora pont ezt teszi, ezért explicit ezeket nem kell kiadnunk
* A külső ciklus feltétele a fejelemes lista "szemantikus" ürességét vizsgálja
* Most nem szórakozunk előző pointerekkel, mert vannak visszapointereink
* Megintcsak a fejelem a kezdeti max, és az azt követő - ami lehet hogy nem létezik, ami itt azzal ekvivalens, hogy visszamutat a fejelemre - lesz az első amit utána bejárunk
* Amikor megvan a max, az előzőjének a következőjét és a következőjének az előzőjét is frissítjük (azaz kiláncoljuk a listából)
* Ezután beláncoljuk a listába, ami első megközelítésben egy ilyen elágazás volt, ami később alakult, alább látható az egyszerűsített változat, merthogy a különben ág általánosabb a felső ágnál (mivel spec. rf->next == rf)
* Végül beállítjuk a fejelem pointerét és töröljük a régit. Ez is egyszerűsödött kicsit (nem kell kimenteni segédváltozóba a fejelemete, mert rf-ben már megvan, minden ami kell)
* Íme a javított változat:

```
maximumSelectionSort(&f : E2C*)
	rf = new E2C
	AMÍG f->next != f
		max = f->next
		p = max->next
		AMÍG p != f
			HA p->key > max->key
				max = p
			p = p->next
		max->next->prev = max->prev
		max->prev->next = max->next
		max->next = rf->next
		rf->next->prev = max
		rf->next = max
		max->prev = rf
	delete f
	f = rf
```
