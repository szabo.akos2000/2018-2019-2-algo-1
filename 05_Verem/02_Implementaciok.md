# Referenciaimplementációk a verem típushoz

* Az előbbiekben megadott interfészt millióféleképpen meg tudnánk valósítani. Két nevezetes példa következik most
* Egy típusimplementációhoz két dolog kell:
	* Reprezentáció: ezek azok a "változók", amik segítségével a típus tetszőleges belső állapotát el tudjuk tárolni. Ezek (többnyire) privát változók, a külvilág számára se a létük, se a tartalmuk nem hozzáférhető, csak közvetett módon az interfészműveletek segítségével módosíthatják azokat (dat is "enkapszuláció" bitches)
	* Műveletek implementációi: az interfészben felsorolt függvények törzsei. A függvények fejeit rendre a "Stack::" előtaggal látom el, ami azt jelenti, hogy a Stack (verem) típuson vannak értelmezve, azaz egy verem példányon hívhatók meg. Ilyenkor implicite van egy plusz paramétere, az a verem, amin meg lett hívva (programozási nyelvekben gyakran "this" vagy "self"). Az ilyen függvényeket hívjuk műveleteknek. Kizárólag ezek a függvények férnek hozzá a verem reprezentációjához

## Aritmetikai (tömbös) ábrázolás

* Reprezentáció: a[1..n], sp eleme [0..n]
	* A verem tehát egy fix méretű tömbbel lesz ábrázolva, aminek a maximális mérete, azaz a verem kapacitása n. Ebből az első sp-nyi elem lesz használva, ez az sp hosszú prefix lesz tehát a verem tartalma a gyakorlatban. Kezdetben az sp értelemszerűen 0, a tömb tartalma pedig épp ezért irreleváns
* Megadok néhány műveletet a tömbös reprezentációval:

	```
	Stack::Stack()
		sp = 0
	```

	```
	Stack::push(x : T)
		HA sp == n
			HIBA
		KÜLÖNBEN
			sp = sp+1
			a[sp] = x
	```

	* A "hiba" feltétele megegyezik az isFull() implementációjával, hiszen egy tele verembe már nem rakhatunk új elemet

## Láncolt megvalósítás

* Reprezentáció: sp eleme E1*
	* Az sp a stack pointer (veremmutató) rövidítése, a mindenkori első elemre fog mutatni, ami kezdetben nyilván NULL. Sima, egyirányú láncolt listával adhatjuk tehát meg
* Néhány művelet:

	```
	Stack::Stack()
		sp = NULL
	```

	```
	Stack::push(x : T)
		p = new E1
		p->key = x
		p->next = sp
		sp = p
	```

	* Megjegyzések:
		* A művelet nem más, mint a "lista elejére szúrás", hiszen ez konstans műveletigényű
		* Figyeljünk a sorrendre! Sose hagyjuk, hogy egy heapbeli változóra egy pointer se mutasson
		* Előfeltétele a "!isFull()", ami itt azonosan igaz
	
	```	
	Stack::pop() : T
		HA sp == NULL
			HIBA
		KÜLÖNBEN
			p = sp
			sp = sp->next
			x = p->key
			delete p
			return x
	```
	
	* Megjegyzések:
		* Az őrfeltétel nem más, mint az isEmpty() itteni implementációja
    	* Ennek a műveletnek háromféle variációja is van, ez a sima függvényszerű
		* A lista elejéről fűzzük ki az elemet, s miután az értékét lementettük, ki is törölhetjük. A mentett értékkel térünk vissza
		* A műveletigény konstans - az összes műveletnél ezt várjuk el!

	```
	Stack::top(&x : T)
		HA sp == NULL
			HIBA
		KÜLÖNBEN
			x = sp->key
	```
	
	* Megjegyzés:
		* Ez most az eljárásszerű megvalósításra vett példa
	
	```
	Stack::isEmpty() : L
	  return sp == NULL
	```

	```
	Stack::isFull(&l : L)
	  l = false
	```
	
	* Megjegyzés:
		* Értelemszerűen nem lehet tele
