# Matematikai kifejezések kiértékelése

* Legyen az alábbi kifejezés a példánk:
	* x := ( 14 - 2 * 2 ^ 3 ^ 2 / 256 ) / 5 * 6 + 2 – 12 / 3
* Ez a hagyományos, emberi fogyasztásra alkalmas, ún. infix forma - azaz a műveleti jel (operátor) a paraméterei (operandusok) között van
* Tudjuk, hogy is kell ezt kiértékelni: ami a zárójelben van, azzal kezdünk, amúgy meg a precedenciakülönbséget nézzük a műveleteknél:
	* A hatvány a legerősebb
	* Utána jön a szorzás és az osztás
	* Majd az összeadás-kivonás
	* Végül az értékadás
* Egyenlő precedenciánál a hatvány és az értékadás esetében jobbról balra értékeljük ki az azonos erejű műveleteket (jobbasszociativitás), míg a többit balról jobbra (balasszociativitás)
* A használható műveleti jelek, precedenciák, asszociativitások persze akár kívülről is konfigurálhatók egy ezt megoldó program esetében
* Ha most teljesen bezárójeleznénk a fenti kifejezést, gyakorlatilag nem kellene törődnünk ezekkel, mert csak a zárójelek mentén kellene haladni:
	* ( x := ( ( ( ( ( 14 - ( ( 2 * ( 2 ^ ( 3 ^ 2 ) ) ) / 256 ) ) / 5 ) * 6 ) + 2 ) – ( 12 / 3 ) ) )
* Innen a megoldás (kiértékelés) menete a következő:
	* Olvassuk be egyesével a karaktereket és dobáljuk be egy verembe. A csukó zárójelt nem rakom be a verembe, hanem amikor ilyet olvasok, kiveszek mindent a nyitó zárójelig - ha helyes a zárójelezés, akkor a két operandus lesz és közte az operátor -, de még a nyitó zárójelt is, kiértékelem ezt az egyszerű binér kifejezést, és visszarakom a verembe
	* Azt nem árt tudatosítani, hogy az első elem amit kiveszek, a jobb operandus, utána az operátor, aztán a bal operandus
	* A végén a megoldás lesz a veremben
* A teljesen zárójelezett fenti példa alapján (a verem állapotát írom ki rendre, amikor csukó zárójelt olvasok - elforgatva írom: bal oldalon a verem alja, és jobbra a veremtető)
	* v=[ ( x := ( ( ( ( ( 14 - ( ( 2 * ( 2 ^ ( 3 ^ 2
	* v=[ ( x := ( ( ( ( ( 14 - ( ( 2 * ( 2 ^ 9
	* v=[ ( x := ( ( ( ( ( 14 - ( ( 2 * 512
	* v=[ ( x := ( ( ( ( ( 14 - ( 1024 / 256
	* v=[ ( x := ( ( ( ( ( 14 - 4
	* v=[ ( x := ( ( ( ( 10 / 5
	* v=[ ( x := ( ( ( 2 * 6
	* v=[ ( x := ( ( 12 + 2
	* v=[ ( x := ( 14 - ( 12 / 3
	* v=[ ( x := ( 14 - 4
	* v=[ ( x := 10
	* v=[ 10 (és mellékhatásként x értéke 10 lett)
