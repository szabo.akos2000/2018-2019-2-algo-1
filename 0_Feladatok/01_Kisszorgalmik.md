# Info

* Minden "határidő"-t értsünk úgy, hogy a megadott nap 23:59 időpontjáig adható le a kész munka
* A megoldást a canvas felületen adjátok le (méltányolható okokból lehet kivétel, ekkor emailben kérem)
* A határidőn túl beadott megoldásokat is átnézem, canvasban leírom, milyen lett - tanulni lehet belőle -, de arra pontot már nem adok
* Amennyiben megcsúszok a félév végén, a globális határidő mindenre: 2019. 07. 05., 16:00
* Hacsak más nincs mondva, minden feladat 1 pontot ér

# 2019. 02. 20. - Második hét, második óra

## 01_BubbleSort

* Határozzuk meg a tanult buborékrendezés algoritmusra az mcsere<sub>bubbleSort</sub>, az Mcsere<sub>bubbleSort</sub> és az Acsere<sub>bubbleSort</sub> értékeket - mindhárom helyes: 1 pont
* Bizonyítsuk is be a fentieket (lehet "esszé-jellegű", de legyen pontos, alapos) - mindhárom helyes: 1 pont
* Határidő: 2019. 03. 20.

# 2019. 02. 27. - Harmadik hét, harmadik óra

## 02_Legendre

* Határozzuk meg a tanult Legendre-algoritmus alapján az mszorzás<sub>legendre</sub>(a, k), Mszorzás<sub>legendre</sub>(a, k) és Aszorzás<sub>legendre</sub>(a, k) értékeket
* Indokoljunk is meg a fentieket konkrét a-kkal, k-kkal, levezetve, hogy jött ki a válasz
* Az algoritmusban látható osztás nem számít szorzásnak, csak a "csillag"-gal jelölt műveletet számoljuk
* Határidő: 2019. 03. 20.

# 2019. 03. 06. - Negyedik hét, negyedik óra

## 03_ThetaEkvRel

* Bizonyítsuk be, hogy a Θ-viszony ekvivalenciareláció
    * reflexív
    * tranzitív
    * szimmetrikus
* Határidő: 2019. 05. 20.

## 04_OrdoOmegaEkvRel

* Vajon ekvivalenciareláció-e Ο és Ω? Döntsük el, és bizonyítsuk!
* Határidő: 2019. 05. 20.

# 2019. 03. 20. - Hatodik hét, hatodik óra

## 05_TorlesSL

* Írjuk meg a delete(&l : E1*, k : T) : L függvényt
    * l egy egyszerű láncolt lista (S1L), ami rendezett
    * Törölje a k kulcs első előfordulását tartalmazó E1-et
    * Térjen vissza egy boollal, ami igaz, ha sikerült a törlés (azaz volt k kulcsú elem)
    * Nyilván a törlést megelőzi egy keresés, ezt rátok bízom milyen módszerrel, hány pointerrel oldjátok meg
* Határidő: 2019. 05. 20.

# 2019. 03. 27. - Hetedik hét, hetedik óra

## 06_TorlesHL

* Írjuk meg a delete(f : E1*, k : T) : L függvényt
    * f egy fejelemes láncolt lista (H1L), ami rendezett
    * Törölje a k kulcs első előfordulását tartalmazó E1-et
    * Térjen vissza egy boollal, ami igaz, ha sikerült a törlés (azaz volt k kulcsú elem)
    * Nyilván a törlést megelőzi egy keresés, ezt rátok bízom milyen módszerrel, hány pointerrel oldjátok meg
* Határidő: 2019. 05. 20.

## 07_Beszuras2L

* Írjuk meg az insert(&l : E2*, q : E2*) függvényt
    * l egy fejelem nélküli, rendezett kétirányú lista (S2L)
* Tipp: Nem kell két pointer a kereséshez, hiszen van visszairányú mutatónk
    * Viszont azt sem szabad elfelejteni frissíteni!
* Határidő: 2019. 05. 20.

## 08_TorlesH2L

* Írjunk függvényt, ami egy kétirányú, fejelemes listából törli a megadott kulcsú elemet
* A rendezettség rátok bízva
* A visszatérési típus és annak értelmezése is
* Határidő: 2019. 05. 20.

## 09_InsertionSort

* Írjuk meg az insertionSort algoritmust egyszerű listára (S1L)
* Határidő: 2019. 05. 20.
