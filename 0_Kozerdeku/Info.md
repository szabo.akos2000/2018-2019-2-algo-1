# Közérdekű közlemények

## Alapadatok

* Az itt található anyagok az Algoritmusok és Adatszerkezetek 1. című tárgy 2018/19. tavaszi félévének 7. és 12. csoportjának szólnak
* 7. csoport: szerdánként, 18.15 - 19.45, D 0-221
* 12. csoport: szerdánként, 16.30 - 18.00, D 00-113
* Mint látható, a két kurzus órái egymás után kerülnek megtartásra. Mivel eleve célom, hogy a két órán ugyanazt az anyagot adjam le, van lehetőség a másik csoport órájára járni (akár hetente váltakozva is, akár mindkét órára is), az alábbi két feltétellel:
    * Ha nincs hely, annak van elsőbbsége, aki az adott órára jár papír szerint
    * Ha zh van az adott héten, írj előtte, s várd meg a visszajelzésem, mert ott nem akarom hagyni, hogy valakinek ne jusson kényelmesen hely
* Ha bármi miatt kérem, hogy jelezd, melyik órára jársz hivatalosan ill. a gyakorlatban, akkor kérlek ne "szerdai"-ként hivatkozz rá, mert mint láthatod mindkettő az

## Elérhetőség

* Email: szitab@ik.elte.hu
* Körüzeneteket neptunból fogok küldeni, legyen megadva email-cím, hogy emailben is megkapd
* Beadandó-beadások, személyre szabott üzenetek, eredmények: https://canvas.elte.hu (belépés neptunnal)
* Közös információ, tananyag, beadandó-feladatkiírások: https://gitlab.com/elte-ik-algo/2018-2019-2-algo-1/
* Külön, automatikus konzultációs időpont nincs, kérdés esetén emailben elérhető vagyok és ha online nem tudjuk megoldani, lehet kérni személyes konzultációt egyedi időpontra

## Követelmények

### Órák

* Jelenlét nem kötelező
* Az első óra kivételével mégis számon fogom tudni tartani a bejárási statisztikákat, s akik kitartóbban látogatják az órát, félév végén esetleges két jegy közötti állásnál, jobb elbírálásban lehet részük (az mindegy, hogy kettesért vagy négyesért küzd az illető)
* Amennyiben valamelyik óra a csoportok tagjainak zömét érintő (évfolyamszintű) egyéb megpróbáltatással (analízis zh) ütközik, az adott órát elhalasztjuk. Ezt kérem, előre jelezzétek! Nemzeti ünnep, stb. miatt elmaradó órát is szeretnék utólag bepótolni. Összesen 13 db órával számolok. A pótórák idejéről szavazással döntünk
* Első órai többségi szavazás alapján végül abban állapodtunk meg, hogy minden olyan anyagot, amit úgy érzek, hogy a kurzus elsajátításához szükséges, alaposan leadok és ide fel is töltök. Ezzel azt kockáztatjuk, hogy nem érünk az anyag végére. Ha ez így történik, a bele nem férő anyagrészt zh-n nem fogom visszakérni, ám szorgalmi feladatokat írhatok ki hozzá, és mivel a vizsgán előfordulhatnak, segédanyagot is fogok feltölteni ezekhez
* Elképzelhető még több segédanyag felöltése is, ezeket érdemes átolvasni, felhasználni, de a kurzus elvileg nélkülük is teljesíthető. Új ismereteket nem fognak hordozni, csak a tanultakat ismételni, elmélyíteni

### Szorgalmi feladatok

* A félév során összesen 10 alkalommal (első óra és zh-k alkalmával nem) egy rövid "kvíz", amire az alábbi pontokat lehet kapni:
    * 0 pont - ha nincs beadva (~hiányzás)
    * 2 pont - ha be van adva és hibátlan
    * 2 pont - ha nincs a csoportban legalább 5 db hibátlan, s a kvíz az összes csoporttagot számolva előbb a helyesség, másodlagosan a gyorsaság szempontjából a legjobb 5 megoldás között van
    * 1 pont - különben
* Bizonyos órai kérdésekre a leggyorsabban jó választ adó (szóban vagy táblánál) kaphat 1 pontot
    * Ezt a lehetőséget a konkrét feladat ismertetése előtt bejelentem
    * Kb. 5 ilyen alkalom várható a félévben
* Órákon változó mennyiségben feladok egy-egy "papíros" szorgalmit 1-1 pontért
    * Kb. összesen 20 db várható a félévben
    * Ezek pontos szövegezése itt, a "0_Feladatok" mappa alatt lesz majd olvasható, beadni pedig a kurzushoz tartozó canvas-felületen kell, akár mint üzenet, akár mint csatolt fájl
    * A határidő a canvason megadottak szerint alakul, ami várhatóan a feltöltés + 1 hét
* A két zh környékén azokhoz kapcsolódóan kódolós szorgalmi feladatok kerülnek kiírásra
    * Mindkét témához 5-5 feladatból lehet szabadon választani egyet-egyet
    * A kiírás és a beadás hasonlóan alakul, mint az előző pontban
    * A határidőre, jó minőségben beadott megoldás értéke 5 pont
    * A gyanúsan hasonló megoldások 0 pontot érnek (esetleges eltérés ettől utólag kialakulhat, ha egyértelműen kiderült, hogy ki a forrás és ki a másoló)
    * A gyengébbnek értékelt beadandókat várhatóan még lehet javítani a visszajelzéseim alapján
    * A pontozás során általános szoftvertechnológiai szempontok és az algoritmus helyessége (és a tanultaknak megfeleltsége) fog számítani
    * Dokumentációt nem kell írni (amíg minden egyértelmű), a nyelv tetszőleges

### Zárthelyik

* 2 db zárthelyi lesz 50-50 pontért a félév közepe felé ill. az utolsó órán
* Mindkettő az óra helyszínén és idejében, annak első 45 percében (utána az óra új anyagrésszel folytatódik; az utolsó héten a vizsgára készülés jegyében)
* A zh-k előtt mindenképp lesz konzultáció, melyre az adott zh-ra gyakorló feladatokkal készülök. Ezek pontos idejét szavazással döntjük el, de várhatóan a zh-t megelőző napok egyik délutánján lesz esedékes
* "Tételsort" és minta zh-t elérhetővé teszek, de a zh feladattípusok jellegéből adódóan a valós zh a mintától igencsak eltérhet
* Szerepelhet tanult algoritmust megadó, azt elemző, azt lejátszó feladat vagy olyan kreatívabb feladat, amit bár órán egy az egyben nem vettünk, de a tanultak alapján megoldható
* 3-4 feladat várható, bár egy feladatnak több része is lehet (lejátszás, elméleti kérdés, bizonyítás, vagy legalábbis indoklás, struktogram...)
* A zh-ra készülésnek mindenképp legyen része az előadás-jegyzet átolvasása is; bár direkt csak előadás-anyagot nem fogok kérdezni, de a tanuláshoz jól jöhet
* Minimális elérendő pontszám nincs, ha anélkül is összejön a szükséges összpontszám, még megírni se kell
* Mindkét zh-t egyszer lehet javítani (akár egyiket, másikat, mindkettőt). Rontás esetén az eredeti pontszámot veszem figyelembe
* Ha a szükséges pontszám így se jön össze, még egyszer meg lehet írni a zh-kat, de ekkor kötelezően a kettőt egyben 100 pontért (~gyakorlati utóvizsga)

### Egyéb

* A részvétel az 5vös5km futóversenyen (szólj róla nekem!) 5 pontot ér
* Minden a honlapon található hiba első megtalálójának (emailben vagy itt Merge Requestként lehet jelezni) 1 pont jár (akár tartalmi, akár csak helyesírási)

### Jegyszerzés

* A fentieknek megfelelően a szorgalmi időszak utolsó napjáig előáll egy összpontszám
* Ezt pótzh-val és azok sikertelensége esetén gyakuv-val lehet majd még javítani
* Ha csak egy-két pont hiányzik, a szorgalmasabbaknak fel fogok ajánlani olyan lehetőséget, hogy (lejárt, be nem adott) szorgalmik közül 4-5 db elkészítésével megadom a jobb jegyet
* A szorgalmi időszak végén írni fogok egy körlevelet, amiben jelzem, a végső pontszámok megszülettek. Arra kérek mindenkit, hogy akár elfogadja ezt a "megajánlott jegyet", akár nem, írjon vissza. Amennyiben elfogadja, a jegy bekerül a neptunba és csakis ez után az illető vizsgára jelentkezhet. Amennyiben nem fogadja el, írja meg, melyik zh-ból javítana, és mikor (lehetőleg több időpont-javaslatot is megjelölve). Hamarosan vissza fogok írni a pótzh idejéről és helyéről. A pótzh ha legalábbis a hallgató ezt igényli, a vizsgaidőszak első két hetében kerül megrendezésre. Ha később szeretné, ez nekem is megfelel, de akkor addig nem mehet el vizsgázni sem
* Gyakorlati jegy nélkül ne menjetek vizsgázni
* Ha a saját hibámból valakinek a szorgalmi időszak végére nincs kialakult gyakorlati jegye az alábbi kedvezményeket nyújtom "cserébe":
    * A beadható "kicsi" és "kódolós" szorgalmi feladatok határideje annulálódik, a vizsgaidőszak utolsó napjáig be lehet adni bármit, és bármeddig lehet javítani
    * El lehet menni előre vizsgázni, igazolom a nem beírt gyakorlati jegyet - itt persze, ha a félév legvégéig nem lesz meg a jegy, a vizsgajegy is törlődni fog!
* A jegyszerzésnél az alábbi ponthatárokat veszem figyelembe (félév során elért összes pont):
    * 100-csillagos ég (kb. 160) - jeles
    * 85-99 - jó
    * 75-84 - közepes
    * 60-74 - elégséges
    * 0-59 - elégtelen
