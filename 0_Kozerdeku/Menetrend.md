# Algoritmusok és Adatszerkezetek 1.

## 2019. 02. 13. - Első hét, első óra

* Témák
    * Követelmények, szabályok
    * Bevezetés, motiváció, a két félév tartalma
    * Adatszerkezetek
    * Algoritmusok
* Fájlok az oldalon
    * 0_Kozerdeku/Info.md
    * 01_Bevezetes/01_Adatszerkezetek.md
    * 01_Bevezetes/02_Algoritmusok.md

## 2019. 02. 20. - Második hét, második óra

* Témák
    * Algoritmusok kiegészítés
    * Pointerek, tömb típus, paraméterátadás
    * Lépésszámmal kapcsolatos jelölések
    * Buborékrendezés lejátszás, algoritmus, elemzés
    * Hanoi tornyai lejátszás, algoritmus

## 2019. 02. 28. - Harmadik hét, harmadik óra

* Témák
    * Hanoi tornyai elemzés
    * Polinom kiértékelése alapfeladat és 3 stratégia műveletigény szempontjából vett összehasonlítása
    * Legendre-algoritmus
    * Műveletigény matematikájának bevezetése (Ο, ο, Ω, ω és Θ definíciói)