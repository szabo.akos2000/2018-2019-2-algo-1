# Buborékrendezés tömbre

## Elv

* Legyen adott egy valamilyen rendezéssel rendelkező alaptípus feletti tömb. Az órai példa a karakter típus volt az ábécérend szerinti rendezéssel. Itt most az egész számok. A feladat az, hogy rendezzük növekvő sorba a tömb elemeit
* Az alapelgondolás a következő: végigmegyünk sorban az összes elemen, és ha ő és a rákövetkezője között inverzió van, megcseréljük őket

* Példa
	* a<sub>1</sub> = [3, 1, 2, 5, 4]
		* [3 1] 2 5 4
		* 1 [3 2] 5 4
		* 1 2 [3 5] 4
		* 1 2 3 [5 4]
		* 1 2 3 4 5
	* Látható, hogy ha mindig a két bekeretezett elemet vizsgálom és ezt a "keretet" minden lépésben eltolom, végül ha pl. a legnagyobb elem volt az első indexen, ez az utolsó lépésre valóban a leghátsó indexre kerül. Ezt szokták "felbuborékoltatásnak" nevezni, innen a rendezés neve
	* De azért ez nem ilyen egyszerű, ha pl. az első három indexen levő elem között nincs ott az első három érték, máris nem elég csak ennyit tenni
* Példa
	* a<sub>2</sub> = [3, 1, 5, 2, 4]
		* [3 1] 5 2 4
		* 1 [3 5] 2 4
		* 1 3 [5 2] 4
		* 1 3 2 [5 4]
		* [1 3] 2 4 | 5
		* 1 [3 2] 4 | 5
		* 1 2 [3 4] | 5
		* [1 2] 3 | 4 5
		* 1 [2 3] | 4 5
		* [1 2] | 3 4 5
		* 1 | 2 3 4 5
	* Itt nem volt elég az első példa első köre. Mivel azt már láttuk, hogy egy futásra az egész tömb legnagyobb eleme felbuborékoltatott az utolsó helyre, ezért az első kör után mondhatjuk, hogy az utolsó indexből álló egy elemű résztömb már rendezett. Sőt ennél többet is mondhatunk: rendezett, és az egész tömböt tekintve a legnagyobb elemek vannak itt, vagy másképp fogalmazva, ezek az elemek már a majdani teljes rendezett tömböt tekintve a helyükön vannak
		* Érdemes ilyen invariáns (a ciklus minden lépése után fennálló) állításokat megfogalmazni, mert segíthet az algoritmust kitalálni, helyességét alátámasztani, elvét memorizálni
		* Itt tehát kimondhatjuk, hogy a ciklus invariánsa k eleme [1..n-1]-re, hogy ha az utolsó k elem már le van választva, az utolsó k elemből álló résztömbben a k legnagyobb elem van és ezek egymáshoz képest rendezettek
		* Innen könnyen kitalálható a külső ciklus intervalluma: n-1 kört kell menni, mert ha "csak" az invariánst sikerül végig fenntartani, az utolsó körre megkapjuk, hogy az utolsó n-1 helyen megvan az n-1 legnagyobb elem, míg a még rendezetlen résztömb önmagában rendezett (nem is tudnám rendezni, mert nincs rákövetkezője, a nem leválasztott részben). Mivel mind az n-1 mögötte levő elem nagyobb nála, szükségszerűen ez az első elem a globális legkisebb, tehát az első indexen levő elem is a helyén van
        * Ha a ciklussal n-től megyünk 2-ig visszafelé egyesével, a ciklusváltozó meg tudja mondani az első a kör végére leválasztott elem indexét. Azaz az első kör után egy elem kerül a helyére, ez lesz a j=n. Utána az utolsó 2, ez a j=n-1, stb. A végére pedig az utolsó n-1 db elem a j=n-(n-2), azaz j=2
	* A belső ciklusban pedig az lesz a dolgunk, hogy páronként hasonlítsuk az elemeket, mégpedig úgy, hogy a leválasztott elemeket már ne vizsgáljuk
		* Ezért kell itt j-1-ig menni. A j jelenti, hogy hol tartunk a külső ciklusban. Mivel most járunk j-nél, ezért a kör végére a j. elem már le lesz választva, de most még nincs. Viszont majd mindig az i. és az i+1. elemeket hasonlítjuk össze, azaz ha az a célunk, hogy a j. elem legyen a legnagyobb, amit vizsgálunk, akkor max. j-1-ig mehetünk
		* Ha j kezdetben n, akkor frankón kijön, hogy kezdetben mind az n elemet néznünk kell
	* Azt is láthatjuk, hogy ezt a két egymásba ágyazott ciklust addig kell ismételnünk, amíg el nem fogy a tömb kezdőszelete, mert addig nem lehetünk biztosak, hogy nincs inverzió. Azaz bár az a<sub>1</sub>-es példánál szabad szemmel is látható hogy már egy kör után is rendezetté vált, de természetesen az algoritmus pont ugyanannyi vizsgálatot fog megtenni ott is, mint az a<sub>2</sub> esetében, hogy ezt tényleg ki is merje mondani

## Algoritmus

* Az alábbi struktogrammal megadva:

```
bubbleSort(a : T[n])
    j = n down to 2
        i = 1 to j-1
            HA a[i] > a[i+1]
                swap(a[i], a[i+1])
```
	  
* Mi lehet a swap (csere) formális paraméterlistája, szignatúrája?
	* Válasz: swap(&x : T, &y : T)
		* A referenciajelet a formális és csakis a formális paraméterlistában ki kell rakni, ha és csakis ha az átadott változónak értéket adunk s ezt "kifelé láttatni akarjuk"
		* Az "a[i]" alakú formális paraméternév nem lenne helyes, mert egyrészt az egy kifejezés, nem pedig egy most bevezetendő álnév-változó, másrészt meg azt sugallná, csak az "a" tömb elemei között valid elképzelés a csere. Pedig bármely két változó között értelmes ez a szándék - és szeretjük a dolgokat általánosan megfogalmazni
		* Mivel a egy T feletti tömb, ezért értelemszerűen x és y álnevekhez a T típus járul
		* Mivel a formális és az aktuális (itt most a[i], a[i+1]) paraméterlista között csak a paraméterek száma, sorrendje, típusa a megfeleltetés alapja, bárhogy hívhatjuk x-et a formális listában. Akár a-nak is, pedig az a meghívás helyén a tömböt jelentette, nem annak egy elemét

## Hatékonyság

* Két egymásba ágyazott ciklusról van szó, a külső n-1 kört megy, a belső meg ahhoz képest megy annyit, amekkora a nem leválasztott rész mínusz egy
* Konkrét példa az összehasonlítások számára (azaz, hogy hány [] keretet írtam le)
	* összehasonlítás<sub>bubbleSort</sub>(5) = 4 + 3 + 2 + 1 = 10
* Általánosan tehát: (n-1) + (n-2) + ... + 1 = (számtani sorozat) = [n(n-1)]/2 = (1/2)n<sup>2</sup> - (1/2)n
	* Azaz ez egy polinom. Erre szokták mondani, hogy polinomiális futási idő
	    * Mivel ez egy négyzetes polinom, ezért ezt konkrétabban négyzetes komplexitásnak fogjuk hívni később
	* Nyilván ez az összefüggés érvényes minden n méretű bemenetre, azaz mösszehasonlítás<sub>bubbleSort</sub>(n) = Mösszehasonlítás<sub>bubbleSort</sub>(n) = Aösszehasonlítás<sub>bubbleSort</sub>(n) s így: = összehasonlítás<sub>bubbleSort</sub>(n)
* A cserék száma az eredeti tömbbeli inverziók számával azonos, és erre már nem igaz a fenti állítás, függ az inputtól a konkrét szám:
	* csere<sub>bubbleSort</sub>(a<sub>1</sub>) = 3
	* csere<sub>bubbleSort</sub>(a<sub>2</sub>) = 4
    * Az inverziók számát úgy lehet kiszámolni, hogy minden elemre megnézem, hány darab őt követő kisebb érték van, és ezeket összegzem. Konkrétan:
        * inverziók(a<sub>1</sub>) = 2 + 0 + 0 + 1 + 0
        * inverziók(a<sub>2</sub>) = 2 + 0 + 2 + 0 + 0