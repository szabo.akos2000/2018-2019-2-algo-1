# Polinomszámítási stratégiák összehasonlítása a műveletigény szempontjából

## Az alapfeladat

* Adott egy n-edfokú polinom az együtthatókkal, mint vektor és az x ismeretlennel, mint szám. Számoljuk ki az értékét!
* A megoldandó egyenlet alakja tehát: z<sub>n</sub> * x<sup>n</sup> + z<sub>n-1</sub> * x<sup>n-1</sup> + ... + z<sub>1</sub>*x + z<sub>0</sub>
* Ebből az együtthatókat egy 0-tól n-ig indexelt (tehát n+1 elemű) "z" nevű tömbbel adjuk meg, az x-et pedig egy külön paraméterként
    * Ne feledjük a konvenciót: a z-re végződő nevű tömb típusú változók 0-tól indexelődnek
* 3 megoldási stratégiát fogunk összehasonlítani. Mindhárom helyes, a különbség a műveletigényben lesz. A szorzások és összeadások számát is vizsgáljuk
	* Optimalizációs lehetőséget elsősorban az x egyre nagyobb hatványainak számításában tudunk behozni, hiszen felesleges kiszámolni "nulláról" az x<sup>10</sup>-t, ha már az x<sup>9</sup>-t egyszer meghatároztuk

## Naiv megoldás

* Menjünk végig a fenti egyenlet tagjain, mondjuk i-vel n-től 0-ig visszafelé egyesével (ha már így írtuk fel a polinomot)

```
naivPolinomszámolás(z : Q[n+1], x : Q) : Q
    s = 0
    i = n down to 0
        s = s + z[i] * x^i
    return s
```

* Ez helyes i=0-ra is, mert x<sup>0</sup> = 1
* Hány szorzás van?
    * Az i-edik tagnál ki kell számolni x<sup>i</sup>-t, ami (egy a hatványozásra is naiv implementációt feltételezve) i-1 darab szorzást jelent, meg még ehhez hozzá kell szorozni az aktuális együtthatót, így tehát minden körben van i szorzásunk. Tekintsük i-t n-től 1-ig, ekkor a szorzások számára egy számtani sorozatot kapunk: n + (n-1) + (n-2) + ... + 1 = (n(n+1))/2. Igazából ehhez még hozzájön az n=0 esete, ahol bár csak 1-gyel szorzunk be, de fizikailag mégiscsak beszorzunk
* Kész vagyunk a szorzásokkal, van összesen n+1 darab tagunk (mert ennyi együttható van), de mivel 0-ból indultunk ki, ezért mindegyiket hozzá is adjuk egyesével: így tehát van összesen n+1 darab összeadás
* Összegezve:
	* Szorzás<sub>naivPolinomszámolás</sub>(n) = (n(n+1))/2 + 1, azaz egy négyzetes függvény jött ki
	* Összeadás<sub>naivPolinomszámolás</sub>(n) = n + 1, ami lineáris függvény

## Rekurzív eljárás

* Használjuk ki azt az igazságot, hogy x<sup>n</sup> = x<sup>n-1</sup> * x, ha n>0, és x<sup>0</sup> = 1
	* Ugyanúgy végezzük el a szorzásokat, de most a kis hatványoktól kezdjük, és közben indítsunk 1-től (mint multiplikatív semleges elem) egy segédváltozót, amibe x aktuális hatványát akkumuláljuk. Lásd a "rekurzív függvény kibontása" programozási tételt

```
rekurzívPolinomszámolásRossz(z : Q[n+1], x : Q) : Q
    sv = x
    s = z[0]
    i = 1 to n-1
        s = s + sv * z[i]
        sv = sv*x
    return s + sv * z[n]
```

* Egy picit optimalizáltunk (de ez a konkrét algoritmus nem minden esetben számít követendőnek, mert a kód nem lett túl szép és nyilvánvaló ettől - lásd Clean Code alapelvek)
    * sv-t eleve x<sup>1</sup>-től kezdjük - hogy megspóroljuk az első szorzást
    * A végén nem számoljuk ki feleslegesen x<sup>n+1</sup>-et
* Azt az invariánst mondhatjuk ki, hogy a k. kör után s-ben x<sup>k</sup>-ig össze vannak adva a tagok, és sv-ben már megvan x<sup>k+1</sup>
    * Ezért megyünk csak n-1-ig, mert ezzel meglesz x<sup>k</sup>
    * És ezért megyünk 1-től, mert eleve kell x<sup>1</sup>
    * A fenti okoskodás meg kell, hogy szólaltassa bennünk a vészjelzést: mi van ha n=0? (Ezzel az esettel amúgy is mindig foglalkozzunk) - ez a megoldás bizony nem jó
* A helyes algoritmus ez:

```
rekurzívPolinomszámolás(z : Q[n+1], x : Q) : Q
    sv = x
    s = z[0]
    i = 1 to n-1
        s = s + sv * z[i]
        sv = sv*x
    HA n == 0
        return s
    KÜLÖNBEN
        return s + sv * z[n]
```
* Nem árt végiggondolni az invariánsokat és a szélsőeseteket (edge cases), ebből könnyen kiesnek a kódolási hibáink!
* A szorzások száma i=0-ra 0, i=n-re 1, különben (1..n-1) pedig 2 szorzást végzünk: 1 + 2*(n-1) = 2n - 1
* Az összeadandók ezek után ugyanúgy n+1-en vannak, de most mivel már az első összeadandóból indultunk ki, ez csak n darab műveletvégzést jelent
* Tekintsük még külön az n=0 esetet. Itt a ciklusba be se lépünk, a végső utasítás pedig az elágazás miatt se összeadást, se szorzást nem fog tartalmazni. Ez az összeadásnál épp kiadja n-t (ami 0), szorzásnál viszont negatív lenne, ami nem lehetséges. Ezért ott esetszétválasztással adjuk meg a helyes formulát
* Összegezve:
	* Szorzás<sub>rekurzívPolinomszámolás</sub>(n) = 0, ha n=0; 2n-1, különben - ami lineáris
	* Összeadás<sub>rekurzívPolinomszámolás</sub>(n) = n, ez is lineáris

## Horner-séma
	
* Az eredeti egyenlettel ekvivalens az alábbi alak: (...((z<sub>n</sub>*x + z<sub>n-1</sub>)*x + z<sub>n-2</sub>)*x...) + z<sub>1</sub>)*x + z<sub>0</sub>
	* Itt, ha a zárójelek mentén belülről kifelé haladva végezzük el a műveleteket, azt láthatjuk, hogy minden szorzáshoz "tartozik" egy összeadás, illetve nyilván most is n+1 tag van, ami n darab művelet
* Összegezve:
	* Szorzás<sub>hornerPolinomszámolás</sub>(n) = n, lineáris
	* Összeadás<sub>hornerPolinomszámolás</sub>(n) = n, lineáris

```
hornerPolinomszámolás(z : Q[n+1], x : Q) : Q
    s = z[n]
    i = 1 to n
        s = s * x + z[n-i]
    return s
```

* Bár a kivonásokat nem néztük, de az n-szer meghívott n-i számolás is felesleges lehet, ezt az alábbi módon tudjuk optimalizálni:

```
hornerPolinomszámolás(z : Q[n+1], x : Q) : Q
    s = z[n]
    i = n-1 down to 0
        s = s * x + z[i]
    return s
```

* A Horner a legjobb megoldás, bár igazi nagyságrendi ugrás a Naiv és a Rekurzív között van
* Érdemes tudatosítani, hogy bár a legtöbb esetben a műveletigényt leíró függvény paramétere az input(tömb) mérete, de itt az nem n, hanem n+1 lenne. Később látni fogjuk, amit már most is sejthetünk, ennek nagy jelentősége nem lesz
