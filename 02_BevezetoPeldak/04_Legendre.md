# Legendre-algoritmus

* Az alább algoritmus célja a gyors hatványozás

	```
	legendre(a : Q, k : N) : Q
		HA a == 0
			h = 0
		KÜLÖNBEN
			h = 1
		AMÍG k>0
			HA 2 | k
				a = a*a
				k = k/2
			KÜLÖNBEN
				h = h*a
				k = k-1
		return h
	```

* Az algoritmus h-ba a<sup>k</sup>-t számolja ki, a bemenetre a típuson kívül más megkötés nincs
* Megalkotása során kifejezetten a szorzások számának minimalizálása volt a cél, hiszen az egy "drága" művelet volt hajdanán
* Lehet ezen még optimalizálni, ha az a==0 ágban rögtön returnölünk is
